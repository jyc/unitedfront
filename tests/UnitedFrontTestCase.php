<?php
/**
 * @author Jonathan Chan <jchan@icebrg.us>
 * @copyright 2011 Jonathan Chan <jchan@icebrg.us>
 * @license http://www.opensource.org/licenses/bsd-license.php The BSD 2-Clause License
 */
namespace icebrg\unitedfront\tests;

require_once 'icebrg/lib/autoloader.php';
\icebrg\lib\autoloader\register(__DIR__ . '/../../../');

use icebrg\unitedfront\UnitedFront;

/**
 * Tests for the UnitedFront class.
 *
 * @author Jonathan Chan <jchan@icebrg.us>
 */
class UnitedFrontTestCase extends \PHPUnit_Framework_TestCase
{
    const DATA_FOLDER = '/data/UnitedFrontTestCase/';

    /**
     * Tests for the throwing of an InvalidArgumentException when supplied an invalid 'defaultDecoder' callback.
     * @expectedException \InvalidArgumentException
     * @return void
     */
    public function testConstructorInvalidDefaultDecoderException()
    {
        $config = array(
            'defaultDecoder'    =>  'notACallback'
        );

        $uf = new UnitedFront(array(), $config);
    }

    /**
     * Tests for the throwing of an InvalidArgumentException when supplied an invalid 'controllersDir'.
     * @expectedException \InvalidArgumentException
     * @return void
     */
    public function testConstructorInvalidcontrollersDirException()
    {
        $config = array(
            'controllersDir' =>  'nonexistent'
        );

        $uf = new UnitedFront(array(), $config);
    }

    /**
     * Tests routing to an index page.
     * @return void
     */
    public function testIndexRouting()
    {
        $routes['GET']['/'] = array(
            'controller'    =>  'index',
            'template'      =>  'index'
        );

        $config = array(
            'controllersDir'     =>  __DIR__ . self::DATA_FOLDER . 'app/',
            'templatesDir'      =>  __DIR__ . self::DATA_FOLDER . 'tpl/'
        );

        $uf = new UnitedFront($routes, $config);

        $match = $uf->route('GET', '/');

        // we can discard everything else
        list($controller) = $match;

        $this->assertEquals('index', $controller);
    }

    /**
     * Tests routing of named parameters.
     * @return void
     */
    public function testNamedParametersRouting()
    {
        $routes['GET']['/path/:named1/:named2'] = array(
            'controller'    =>  'named',
            'template'      =>  'null'
        );

        $config = array(
            'controllersDir'     =>  __DIR__ . self::DATA_FOLDER . 'app/',
            'templatesDir'      =>  __DIR__ . self::DATA_FOLDER . 'tpl/'
        );

        $uf = new UnitedFront($routes, $config);

        $match = $uf->route('GET', '/path/one/two');

        list($controller, $decoder, $template, $args) = $match;

        $this->assertEquals('named', $controller);

        $this->assertEquals('one', $args['named1']);
        $this->assertEquals('two', $args['named2']);
    }

    /**
     * Tests decoding and rendering of data echoed by the controller.
     * @return void
     */
    public function testRenderingByEcho()
    {
        $routes['GET']['/'] = array(
            'controller'    =>  'helloWorldByEcho',
            'template'      =>  'echo'
        );

        $config = array(
            'baseUri'       =>  '/',
            'controllersDir' =>  __DIR__ . self::DATA_FOLDER . 'app/',
            'templatesDir'  =>  __DIR__ . self::DATA_FOLDER . 'tpl/'
        );

        $uf = new UnitedFront($routes, $config);

        ob_start();
            $uf->go('GET', '/');
        $output = ob_get_clean();

        $this->assertEquals("Hello, world!", $output);
    }

    /**
     * Tests decoding and rendering of data returned by the controller.
     * @return void
     */
    public function testRenderingByReturn()
    {
        $routes['GET']['/'] = array(
            'controller'    =>  'helloWorldByReturn',
            'template'      =>  'echo'
        );

        $config = array(
            'baseUri'       =>  '/',
            'controllersDir' =>  __DIR__ . self::DATA_FOLDER . 'app/',
            'templatesDir'  =>  __DIR__ . self::DATA_FOLDER . 'tpl/'
        );

        $uf = new UnitedFront($routes, $config);

        ob_start();
            $output = $uf->go('GET', '/');
        $output = ob_get_clean();

        $this->assertEquals("Hello, world!", $output);
    }

    /**
     * Tests decoding and rendering of data encoded in a custom format, decoded by a custom decoder.
     * @return void
     */
    public function testRenderingWithCustomDecoder()
    {
        $customDecoder = function($raw) {
            if (is_array($raw)) {
                return $raw;
            }
            
            // a query string decoder
            $parsed = array();

            parse_str($raw, $parsed);

            return $parsed;
        };

        $routes['GET']['/'] = array(
            'controller'    =>  'helloWorldByEchoQueryString',
            'decoder'       =>  $customDecoder,
            'template'      =>  'echo'
        );

        $config = array(
            'baseUri'       =>  '/',
            'controllersDir' =>  __DIR__ . self::DATA_FOLDER . 'app/',
            'templatesDir'  =>  __DIR__ . self::DATA_FOLDER . 'tpl/'
        );

        $uf = new UnitedFront($routes, $config);

        ob_start();
            $output = $uf->go('GET', '/');
        $output = ob_get_clean();

        $this->assertEquals("Hello, world!", $output);
    }

    /**
     * Tests receiving of named parameters.
     * @return void
     */
    public function testRenderingWithNamedParameters()
    {
        $routes['GET']['/:foo'] = array(
            'controller'    =>  'fooNamedParameterByEcho',
            'template'      =>  'echo'
        );

        $config = array(
            'baseUri'       =>  '/',
            'controllersDir' =>  __DIR__ . self::DATA_FOLDER . 'app/',
            'templatesDir'  =>  __DIR__ . self::DATA_FOLDER . 'tpl/'
        );

        $uf = new UnitedFront($routes, $config);

        ob_start();
            $uf->go('GET', '/bar');
        $output = ob_get_clean();

        $this->assertEquals('bar', $output);
    }

    /**
     * Tests routing to different controllers given the same path but a different HTTP method.
     * @return void
     */
    public function testRoutingBasedOnHttpMethod()
    {
        $routes['GET']['/']     =   array(
            'controller'    =>  'index',
            'template'      =>  'index'
        );

        $routes['POST']['/']    =   array(
            'controller'    =>  'index2',
            'template'      =>  'index2'
        );

        $config = array(
            'controllersDir' =>  __DIR__ . self::DATA_FOLDER . 'app/',
            'templatesDir'  =>  __DIR__ . self::DATA_FOLDER . 'tpl/'
        );

        $uf = new UnitedFront($routes, $config);

        $matchGet     =   $uf->route('GET', '/');
        /*
         * Should equal:
         * array(
         *  array(
         *      array('index', defaultDecoder, 'index')
         *  )
         *  array(
         *      array('index2', defaultDecoder, 'index2')
         *  )
         * )
         */
        $matchPost    =   $uf->route('POST', '/');

        $this->assertEquals('index', $matchGet[0]);

        $this->assertEquals('index2', $matchPost[0]);
    }

    /**
     * Tests whether a RuntimeException is correctly thrown when the baseUri is missing from the request path.
     * @expectedException \RuntimeException
     * @expectedExceptionMessage The base URI was not found in the request URI.
     * @return void
     */
    public function testBaseUriNotPresentInPath()
    {
        $routes = array();

        $config = array(
            'baseUri'       =>  '/not/in/path',
            'controllersDir' =>  __DIR__ . self::DATA_FOLDER . 'app/',
            'templatesDir'  =>  __DIR__ . self::DATA_FOLDER . 'tpl/'
        );

        $uf = new UnitedFront($routes, $config);

        $uf->go('GET', '/'); // The exception should be thrown here. / is not in /not/in/path.
    }

    /**
     * Tests handling situations in which no match is found for the path requested.
     * @return void
     */
    public function testNoMatchHandling()
    {
        $routes = array();

        $config = array(
            'baseUri'       =>  '/',
            'controllersDir' =>  __DIR__ . self::DATA_FOLDER . 'app/',
            'templatesDir'  =>  __DIR__ . self::DATA_FOLDER . 'tpl/',
        );

        $uf = new UnitedFront($routes, $config);
    }

    /**
     * Tests whether a RuntimeException is correctly thrown when an attempt is made to load a nonexistent template.
     * @expectedException \RuntimeException
     * @return void
     */
    public function testLoadingNonExistentTemplate()
    {
        $routes = array();

        $config = array(
            'baseUri'       =>  '/',
            'controllersDir' =>  __DIR__ . self::DATA_FOLDER . 'app/',
            'templatesDir'  =>  __DIR__ . self::DATA_FOLDER . 'tpl/',
        );

        $uf = new UnitedFront($routes, $config);

        $old_level = error_reporting(0);
            $uf->runTemplate('nonexistent-template', array());
        error_reporting($old_level);
    }

    /**
     * Tests the loading of a template.
     * @return void
     */
    public function testLoadingTemplate()
    {
        $routes = array();

        $config = array(
            'baseUri'       =>  '/',
            'controllersDir' =>  __DIR__ . self::DATA_FOLDER . 'app/',
            'templatesDir'  =>  __DIR__ . self::DATA_FOLDER . 'tpl/',
        );

        $uf = new UnitedFront($routes, $config);

        ob_start();
            $uf->runTemplate('echo', array(
                                       'data'   =>  'Hello, world!'
                                     ));
        $output = ob_get_clean();

        $this->assertEquals("Hello, world!", $output);
    }

    /**
     * Tests whether null is correctly returned when no routes are supplied and UnitedFront::route() is called.
     * @return void
     */
    public function testRouteNoRoutesSupplied()
    {
        $routes = array();
        
        $config = array(
            'baseUri'       =>  '/',
            'controllersDir' =>  __DIR__ . self::DATA_FOLDER . 'app/',
            'templatesDir'  =>  __DIR__ . self::DATA_FOLDER . 'tpl/',
        );

        $uf = new UnitedFront($routes, $config);

        $this->assertEquals(null, $uf->route('GET', '/'));
    }

/*  Unfortunately this functionality is untestable on the CLI SAPI, where headers are not sent
    public function testMultipleTemplates()
    {
        $routes['GET']['/:code'] = array(
            'controller'   =>  'responseCodeFactory',
            'templates'     =>  array(
                '200'       =>  'echo-one',
                '404'       =>  'echo-two',
                '*'         =>  'echo-three'
            )
        );

        $config = array(
            'baseUri'       =>  '/',
            'controllersDir' =>  __DIR__ . self::DATA_FOLDER . 'app/',
            'templatesDir'  =>  __DIR__ . self::DATA_FOLDER . 'tpl/',
        );

        $uf = new UnitedFront($routes, $config);

        ob_start();
            $uf->go('GET', '/200');
        $this->assertEquals('One', ob_get_clean());

        ob_start();
            $uf->go('GET', '/404');
        $this->assertEquals('Two', ob_get_clean());

        ob_start();
            $uf->go('GET', '/500');
        $this->assertEquals('Three', ob_get_clean());
    }*/

    /**
     * @expectedException \InvalidArgumentException
     * @return void
     */
    public function testExceptionOnNonArrayTemplates()
    {
        $routes['GET']['/:code'] = array(
            'controller'    =>  'helloWorldByEcho',
            'templates'     =>  'not-an-array!'
        );

        $config = array(
            'baseUri'       =>  '/',
            'controllersDir' =>  __DIR__ . self::DATA_FOLDER . 'app/',
            'templatesDir'  =>  __DIR__ . self::DATA_FOLDER . 'tpl/',
        );

        $uf = new UnitedFront($routes, $config);
    }
}

/* End of File UnitedFrontTestCase.php */