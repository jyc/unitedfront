<?php
/**
 * @author Jonathan Chan <jchan@icebrg.us>
 * @copyright 2011 Jonathan Chan <jchan@icebrg.us>
 * @license http://www.opensource.org/licenses/bsd-license.php The BSD 2-Clause License
 */
namespace icebrg\unitedfront;

/**
 * An attempt at a UNIX-y PHP "framework".
 *
 * @author Jonathan Chan <jchan@icebrg.us>s
 */
class UnitedFront
{

    /**
     * The base directory from which this class will be called.
     * @var string
     */
    private $_baseUri;

    /**
     * A decoder that takes a raw value (a controller's return value or output) and returns an associative array of
     * values to extract in a template's scope to use if none is specified for the individual route.
     * @var callback
     */
    private $_defaultDecoder;

    /**
     * The directory in which controllers are stored.
     * @var string
     */
    private $_controllersDir;

    /**
     * The suffix for controller files.
     * @var string
     */
    private $_controllersSuffix;

    /**
     * The directory in which templates are stored.
     * @var string
     */
    private $_templatesDir;

    /**
     * The suffix for template files.
     * @var string
     */
    private $_templatesSuffix;

    /**
     * An array of HTTP methods containing keys (routes) mapped to destinations, which can be
     * a two-value tuple of a controller and a template or a three-value tuple of a controller, a decoder callback,
     * and a template, with controllers being specified either as strings or as arrays of controllers.
     * @var array[string]mixed
     */
    private $_routes;

    /**
     * @throws \InvalidArgumentException If a configuration value is invalid.
     * @param array $routes An array of HTTP methods containing keys (routes) mapped to destinations, which can be
     *      a two-value tuple of a controller and a template or a three-value tuple of a controller, a decoder callback,
     *      and a map of content-types to HTTP response codes to templates.
     * @param array|null $config An associative array of configuration values.
     *      <table>
     *          <tr><th>Name</th><th>Default</th><th>Description</th></tr>
     *          <tr><td>baseUri</td><td>/</td><td>The base directory from
     *                  which this class will be called.</td></tr>
     *          <tr><td>defaultDecoder</td><td>A decoder that takes a raw value (a controller's return value or output)
     *                  and returns an associative array of values to extract in a template's scope to use if none is
     *                  specified for the individual route.</td></tr>
     *          <tr><td>controllersDir</td><td>app/</td><td>The directory in which controllers are stored.</td></tr>
     *          <tr><td>controllersSuffix</td><td>.php</td><td>The suffix for controller files.</td></tr>
     *          <tr><td>templatesDir</td><td>tpl/</td><td>The directory in which templates are stored.</td></tr>
     *          <tr><td>templatesSuffix</td><td>.tpl.php</td><td>The suffix for template files.</td></tr>
     *      </table>
     */
    public function __construct(array $routes, array $config = null)
    {
        if ($config == null) $config = array();

        $defaultConfig = array(
            'baseUri'           => '/',
            'defaultDecoder'    =>
                    function($raw)
                    {
                        if ($raw == null) {
                            return array();
                        }
                        
                        if (is_array($raw)) {
                            return $raw;
                        }
                        
                        $decoded = json_decode($raw, true); // decode as associative array

                        return $decoded;
                    },
            'controllersDir'     =>  'app/',
            'controllersSuffix'  =>  '.php',
            'templatesDir'      =>  'tpl/',
            'templatesSuffix'   =>  '.tpl.php'
        );

        $config = array_merge($defaultConfig, $config);

        // Validate configuration

        if ( ! is_dir($config['controllersDir'])) {
            throw new \InvalidArgumentException("'controllersDir' must be a directory.");
        }

        if ( ! is_dir($config['templatesDir'])) {
            throw new \InvalidArgumentException("'templatesDir' must be a directory.");
        }

        if ( ! is_callable($config['defaultDecoder'])) {
            throw new \InvalidArgumentException("'defaultDecoder' must be a callback.");
        }

        // Sanitize configuration

        $config['baseUri'] = rtrim($config['baseUri'], '/') . '/';

        // Save

        $this->_baseUri             =   $config['baseUri'];
        $this->_defaultDecoder      =   $config['defaultDecoder'];
        $this->_controllersDir      =   realpath($config['controllersDir']) . '/';
        $this->_controllersSuffix   =   $config['controllersSuffix'];
        $this->_templatesDir        =   realpath($config['templatesDir']) . '/';
        $this->_templatesSuffix     =   $config['templatesSuffix'];
        $this->_defaultDecoder      =   $config['defaultDecoder'];

        // Parse routes

        foreach ($routes as $method => $methodRoutes) {
            foreach ($methodRoutes as $route => $to) {
                // trim whitespace and /
                $route = trim(trim($route), '/');

                $decoder = null;
                $controller = null;
                $templates = null;

                if (isset($to['controller'])) $controller = $to['controller'];

                if (isset($to['decoder']))
                    $decoder = $to['decoder'];
                else
                    $decoder = $this->_defaultDecoder;

                if (isset($to['template'])) $templates = array('*' => array('*' => $to['template']));
                if (isset($to['templates']) && is_array($to['templates'])) $templates = $to['templates'];

                if ($controller == null) {
                    throw new \InvalidArgumentException("All handler arrays must contain a 'controller' value!");
                }

                if ($templates == null) {
                    throw new \InvalidArgumentException("All handler arrays must contain a 'template(s)' value!");
                }

                $to = array($controller, $decoder, $templates);

                $parts = $route == '' ? array() : explode('/', $route);

                $this->_routes[$method][] = array($parts, $to);
            }
        }
    }

    /**
     * Routes a given HTTP method and path to an entry in the routes array.
     * @param string $method The HTTP method.
     * @param string $path The path, excluding the baseUri.
     * @return array[int]string|null An array containing the matched controller name as supplied in the routes array,
     *      the decoder to decode the controller's output with, a map of template to render given content-types and
     *      response codes, and an associative array of named parameters and their matched values, or null, if there is
     *      no match.
     */
    public function route($method, $path)
    {
        $path = trim($path, '/');
        $pathParts = $path == '' ? array() : explode('/', $path);

        if ( ! isset($this->_routes[$method]) || ! is_array($this->_routes[$method])) {
            return null;
        }

        foreach ($this->_routes[$method] as $each) {
            list($routeParts, $to) = $each;

            $result = $this->_isMatch($routeParts, $pathParts);

            // if result isn't false, it's the matched arguments
            if ($result !== false) {
                list($controller, $decoder, $templates) = $to;
                return array($controller, $decoder, $templates, $result);
            }
        }

        return null;
    }

    /**
     * Runs a controller then returns its buffered output or return value if available.
     * @throws \RuntimeException If the include()ing of the controller fails.
     * @param string $controller The name of the controller (sans directory and suffix) to run.
     * @param array $args An associative array of values to expand into the $_GET super-global for the duration of this
     *      function.
     * @return array|string The output of the controller or its returned value if available.
     */
    public function runController($controller, $args)
    {
        if ( ! file_exists($this->_controllersDir . $controller . $this->_controllersSuffix)) {
            throw new \RuntimeException("The controller '$controller' does not exist.");
        }

        $originalGet = $_GET;

        $_GET = array_merge($_GET, $args);

        ob_start();
            $return = include($this->_controllersDir . $controller . $this->_controllersSuffix);
        $output = ob_get_clean();

        $_GET = $originalGet;

        if ($return !== 1) {
            return $return;
        // nothing was returned
        } else {
            return $output;
        }
    }

    /**
     * Runs a template.
     * @throws \RuntimeException If the include()ing of the template fails.
     * @param string $template The name of the template (sans directory and suffix) to run.
     * @param array $args An associative array of values to expand into the template's scope for the duration of this
     *      function.
     * @return void
     */
    public function runTemplate($template, $args)
    {
        // We use these ugly names to mitigate naming conflicts with arguments passed to the templates.
        $_uf_toExtract = &$args;
        $_uf_templatePath = $this->_templatesDir . $template . $this->_templatesSuffix;

        if ( ! file_exists($_uf_templatePath)) {
            throw new \RuntimeException("The template '$template' does not exist.");
        }

        // We create a sandbox function to limit scope as much as possible.
        $sandbox = function() use ($_uf_toExtract, $_uf_templatePath) {
            // don't overwrite variables in the case of a name conflict
            extract($_uf_toExtract, EXTR_SKIP);

            include($_uf_templatePath);
        };

        $sandbox();
    }

    /**
     * Matches a method and URI to a controller using the routes array supplied in the constructor, executes it,
     * merging matched named parameters into the $_GET superglobal, then decodes the controller's output before passing
     * it to the appropriate template.
     * @throws \RuntimeException If the decoding of the controller's output fails.
     * @param bool $method The HTTP method.
     * @param bool $uri The full URI.
     * @return array[int]string|null An array containing the matched controller name as supplied in the routes array,
     *      the name of the rendered template, the associative array of named parameters to matched values expanded into
     *      the $_GET superglobal, and the associative array of data expanded in the template, or null, if there is no
     *      match.
     */
    public function go($method = false, $uri = false)
    {
        $method = $method ?: $_SERVER['REQUEST_METHOD'];
        $uri = $uri ?: $_SERVER['REQUEST_URI'];

        // trim the baseUri and query string if present
        if (stripos($uri, $this->_baseUri) === 0) {
            if (! empty($_SERVER['QUERY_STRING']))
                $uri = substr($uri, strlen($this->_baseUri), -1 * strlen($_SERVER['QUERY_STRING']) - 1);
            else
                $uri = substr($uri, strlen($this->_baseUri));
        // if not present, something is wrong...
        } else {
            throw new \RuntimeException("The base URI was not found in the request URI.");
        }

        $match = $this->route($method, $uri);

        // No match
        if ($match == null) {
            return null;
        }

        list($controller, $decoder, $templates, $args) = $match;
        /** @var $decoder \Closure */

        ////////////////////
        // Run controller //
        ////////////////////

        $returned = $this->runController($controller, $args);

        // false returned on error
        $decoded = @$decoder($returned);

        if ( ! is_array($decoded)) {
            throw new \RuntimeException("Failed to decode the controller '$controller''s output.");
        }

        /////////////////
        // Get headers //
        /////////////////

        $headers = array();

        foreach (headers_list() as $header) {
            list($key, $value) = array_map('trim', explode(':', $header, 2));

            $headers[strtolower($key)] = $value;
        }

        $response_code = 200;

        if (isset($headers['status'])) {
            $parts = explode(' ', $headers['status']);
            $response_code = $parts[0];


            // Set status code the non-FastCGI way too
            header('HTTP/1.1 ' . $headers['status']);
        }

        /////////////////////
        // Select Template //
        /////////////////////

        $map = null;
        $template = null;

        if (isset($headers['content-type']) && isset($templates[$headers['content-type']])) {
            $map = &$templates[$headers['content-type']];
        } else {
            if ( ! isset($templates['*'])) {
                throw new \RuntimeException("No template could be matched to any content type, including the * wildcard type.");
            }

            $map = &$templates['*'];
        }

        if ( ! is_array($map)) {
            $template = &$map;
        } elseif (isset($map[$response_code])) {
            $template = &$map[$response_code];
        } else {
            if ( ! isset($map['*'])) {
                throw new \RuntimeException("No template could be matched to response code $response_code.");
            }

            $template = &$map['*'];
        }

        $this->runTemplate($template, $decoded);

        return array($controller, $template, $args, $decoded);
    }

    /**
     * Tests if the given path parts are applicable to the given route parts.
     * @param array $routeParts An array of route parts to test. Values starting with a colon ':' are treated as named
     * parameters.
     * @param array $pathParts An array of path parts to test.
     * @return array|bool An associative array of path parts matched to named parameters or false if there is no match.
     */
    protected function _isMatch($routeParts, $pathParts) {
        $args = array();

        if (count($routeParts) != count($pathParts)) {
            return false;
        }

        for ($i = 0; $i < count($routeParts); $i++) {
            if (substr($routeParts[$i], 0, 1) == ':') {
                $args[substr($routeParts[$i], 1)] = $pathParts[$i];
            } elseif ($routeParts[$i] != $pathParts[$i]) {
                return false;
            }
        }

        return $args;
    }
}

/* End of File UnitedFront.php */