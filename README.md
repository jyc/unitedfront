README
======

* Use the PEAR Coding Standards (http://pear.php.net/manual/en/standards.php) and the One True Brace Style.
* Report issues and contribute at the Bitbucket repository (https://bitbucket.org/jonathanyc/united-front/overview).
* __Take a look at the `bin/uf-scaffold.dist/init/security.php` file - it sets a bunch of security-related HTTP headers
that may cause problems with your application!