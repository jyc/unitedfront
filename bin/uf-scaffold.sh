#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SCAFFOLD_PATH="$DIR/uf-scaffold.dist"

set +e # Immediately break on error

if [ -z $1 ]; then
    echo "Usage:    uf-scaffold <directory>"
    echo "  Creates a base United Front application at the specified directory."

    exit 64
fi;

if [ -d $1 ]; then
    echo "Cannot create application - directory already exists!"
    exit 1
fi;

cp -R $SCAFFOLD_PATH/ $1/
