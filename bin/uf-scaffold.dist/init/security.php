<?php
/**
 * Some simple "tweaks" to increase the security of most applications.
 *
 * @author Jonathan Chan <jchan@icebrg.us>
 * @copyright 2011 Jonathan Chan <jchan@icebrg.us>
 * @license http://www.opensource.org/licenses/bsd-license.php The BSD 2-Clause License
 */

// Remove the X-Powered-By header as protection against script-kiddies
header('X-Powered-By: ');

// Don't allow this page to be put in frames, to protect against CSRF
header('X-Frame-Options: DENY');

// Only allow content from our own domain - disallows inline JavaScript, eval(), data: urls, including images/css/
// JavaScript from a whole bunch of sources, etc. - read https://developer.mozilla.org/en/Security/CSP/CSP_policy_directives
// for more information. You __will__ need to white-list domains whose content you include!
//
// Ex #1 (taken from the above document):
//
//   X-Content-Security-Policy: default-src 'self'; img-src *; media-src media1.com; script-src userscripts.example.com
//
//   A web site administrator wants to allow users of a web application to include images from any domain in their
//   custom content, but to restrict audio or video media to come only from trusted providers, and all scripts only to a
//   specific server that hosts trusted code.
//   Here, by default, content is only permitted from the document's original host, with the following exceptions:
//
//   * Images may loaded from anywhere (note the "*" wildcard).
//   * Media is only allowed from media1.com and media2.com (and not from subdomains of those sites).
//   * Executable script is only allowed from userscripts.example.com.
header("X-Content-Security-Policy: default-src 'self';");

// Turn on XSS protection in IE8+ (see http://blogs.msdn.com/b/ie/archive/2008/07/01/ie8-security-part-iv-the-xss-filter.aspx)
header('X-XSS-Protection: 1; mode=block');

/* End of File security.php */