<?php
/**
 * Should require() other files to call after the autoloader has been loaded and before UnitedFront is called.
 *
 * @author Jonathan Chan <jchan@icebrg.us>
 * @copyright 2011 Jonathan Chan <jchan@icebrg.us>
 * @license http://www.opensource.org/licenses/bsd-license.php The BSD 2-Clause License
 */

require __DIR__  . '/security.php';

/* End of File init.php */