<?php
/**
 * A default "bootstrap" for uf. It is intended that you route all requests for non-existent files in the application
 * directory to this file.
 *
 * It reads routes from "conf/uf/routes.php", and other configuration from "conf/uf/conf.php".
 * The "init.php" in the "init/" directory is require()d after the autoloader is registered.
 *
 * @author Jonathan Chan <jchan@icebrg.us>
 * @copyright 2011 Jonathan Chan <jchan@icebrg.us>
 * @license http://www.opensource.org/licenses/bsd-license.php The BSD 2-Clause License
 */

// -- Configuration Start --

/**
 * Whether or not to catch all errors and exceptions and display a friendly 500 page.
 * @var bool
 */
$debug = true;

/**
 * A bitmask of error levels to catch in the error handler. If -1, all errors will be caught.
 * @var int
 */
$catchErrorLevels = -1;

/**
 * The path to the application folders (app/, conf/, etc.) without a trailing slash.
 * @var string
 */
$applicationPath = __DIR__;

// -- Configuration End --

if ( ! $debug) {
    $display500 = function()
    {
        // Clear the buffered output - we'll just be displaying an error page
        ob_end_clean();

        require __DIR__ . '/tpl/uf/500.php';

        exit();
    };

    // Register the error handler
    set_error_handler(
        function($errno, $errstr, $errfile = null, $errline = null, $errcontext = null) use ($display500)
        {
            $display500();
        }, $catchErrorLevels
    );

    // Register the exception handler
    set_exception_handler(
        function($exception) use ($display500)
        {
            $display500();
        }
    );
}

// Buffer all data, so we can show an error page on error.
ob_start();

// Add the framework root folder, the lib/ folder, and the folder two levels above this folder (the "vendors" folder,
// according to PSR-0) to the include path.
set_include_path(get_include_path() . PATH_SEPARATOR
                 . implode(PATH_SEPARATOR, array(
                                                __DIR__,
                                                __DIR__ . '/lib/',
                                                __DIR__ . '/../../')));

// Register the autoloader.
require 'icebrg/lib/autoloader.php';
\icebrg\lib\autoloader\register();

require $applicationPath . '/init/init.php';

$routes = require $applicationPath . '/conf/uf/routes.php';
$config = require $applicationPath . '/conf/uf/conf.php';

$uf = new \icebrg\unitedfront\UnitedFront($routes, $config);

// Run UnitedFront.
$result = $uf->go();

// UnitedFront::go() returns null if no matching route is found
if ($result == null) {
    require $applicationPath . '/tpl/uf/404.php';
}

// FLush all buffered data if we've gotten this far without error.
ob_end_flush();

/* End of File index.php */