<?php
use icebrg\lib\localization\LocaleTable;
use icebrg\lib\fs\DirectoryAccessor;

$i18n = new StdClass;

// lol
$i18n->_config     =   array(
    'accessFilesAs' =>  DirectoryAccessor::FILE_ACCESS_REQUIRE
);
$i18n->_accessor   =   new DirectoryAccessor(__DIR__ . '/../../inc/messages', $i18n->_config);
$i18n->table       =   new LocaleTable('en', $i18n->_accessor);

return $i18n;

/* End of File i18n.php */